const dbConn = require('./db-connect');

function registerUser(fullname, email, password) {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO t_users (fullname,email, password, is_verified, is_active) VALUES ('${fullname}','${email}', '${password}', 0,0)`;

        isUserExists(email)
            .then((isSuccess) => {
                if (!isSuccess) {
                    dbConn.query(sql, function (err, result) {
                        if (err) {
                            reject(err);
                        }
                        if (result) {
                            if (result.insertId) {
                                resolve(true);
                            }
                            else {
                                resolve(false);
                            }
                        }
                    });
                }
                else {
                    resolve(false);
                }
            }, (err) => {
                reject(err);
            })
    })
}

function isValidateLogin(email, password) {
    return new Promise((resolve, reject) => {
        let sql = `SELECT * FROM t_users WHERE email='${email}' AND password='${password}'`;

        dbConn.query(sql, function (err, result) {
            if (err) {
                reject(err)
            }
            if (result[0]) {
                resolve(result[0])
            } else {
                resolve(null)
            }
        });
    });
}


function isUserExists(email) {
    return new Promise((resolve, reject) => {
        let sql_checkAlreadyExists = `SELECT * FROM t_users WHERE email='${email}'`;

        dbConn.query(sql_checkAlreadyExists, function (err, result) {
            if (err) {
                reject(err)
            }
            if (result[0]) {
                resolve(true)
            } else {
                resolve(false)
            }
        });
    });
}

function getUserFileCount(user_id) {
    return new Promise((resolve, reject) => {
        let sql_checkAlreadyExists = `SELECT COUNT(*) As FileCount FROM t_notarized_files WHERE user_id='${user_id}' and is_notarized=1`;

        dbConn.query(sql_checkAlreadyExists, function (err, result) {
            if (err) {
                reject(err)
            }
            if (result[0]) {
                resolve(result[0].FileCount)
            } else {
                resolve(0)
            }
        });
    });
}

function getNotarizedFiles(user_id) {
    return new Promise((resolve, reject) => {
        let sql_checkAlreadyExists = `SELECT * FROM t_notarized_files WHERE user_id='${user_id}' and is_deleted=0`;

        dbConn.query(sql_checkAlreadyExists, function (err, result) {
            if (err) {
                reject(err)
            }
            if (result[0]) {
                resolve(result)
            } else {
                result = [];
                resolve(result)
            }
        });
    });
}

function addUserFiles(user_id, filename, file_hash = null) {
    return new Promise((resolve, reject) => {

        const d = new Date();
        const timenow = [d.getFullYear(), "-", d.getMonth(), "-", d.getDate(), " ", d.getHours(), ":", d.getMinutes(), ":", d.getSeconds()].join("");

        let sql = `INSERT INTO t_notarized_files (user_id,filename, file_hash, file_created_date, is_notarized, is_deleted) `;
        sql += ` VALUES (${user_id},'${filename}','${file_hash}' ,'${timenow}', 0,0)`;

        dbConn.query(sql, function (err, result) {
            if (err) {
                reject(err);
            }
            if (result) {
                if (result.insertId) {
                    resolve(result.insertId);
                }
                else {
                    resolve(0);
                }
            }
        });
    })
}

function verifyUser(email) {
    return new Promise((resolve, reject) => {
        let sql = `UPDATE t_users SET is_verified=1, is_active=1 WHERE email='${email}'`;

        dbConn.query(sql, function (err, result) {
            if (err) {
                reject(err);
            }
            if (result) {
                resolve(true);
            }
        });
    });
}
function updateUserfile(user_id, file_id, tranx_id) {
    return new Promise((resolve, reject) => {
        let sql = `UPDATE t_notarized_files SET trx_id='${tranx_id}', is_notarized=1  WHERE id=${file_id} and user_id=${user_id}`;
        console.log(sql);
        dbConn.query(sql, function (err, result) {
            if (err) {
                reject(err);
            }
            if (result) {
                resolve(true);
            }
        });
    })
}

function deleteUserfile(user_id, file_id) {
    return new Promise((resolve, reject) => {
        let sql = `UPDATE t_notarized_files SET is_deleted=1 WHERE id=${file_id} and user_id=${user_id}`;
        console.log(sql);
        dbConn.query(sql, function (err, result) {
            if (err) {
                reject(err);
            }
            if (result) {
                resolve(true);
            }
        });
    })
}

function updateUserWalletInfo(user_id, wallet_address) {
    return new Promise((resolve, reject) => {

        let sql = `UPDATE t_users SET wallet_address='${wallet_address}' WHERE id='${user_id}'`;

        dbConn.query(sql, function (err, result) {
            if (err) {
                reject(err);
            }
            if (result) {
                resolve(true);
            }
        });
    })
}
function updateUserProfile(user_id, fullname) {
    return new Promise((resolve, reject) => {

        let sql = `UPDATE t_users SET fullname='${fullname}' WHERE id='${user_id}'`;

        dbConn.query(sql, function (err, result) {
            if (err) {
                reject(err);
            }
            if (result) {
                resolve(true);
            }
        });
    })
}

function updateUserPassword(user_id, password) {
    return new Promise((resolve, reject) => {

        let sql = `UPDATE t_users SET password='${password}' WHERE id='${user_id}'`;

        dbConn.query(sql, function (err, result) {
            if (err) {
                reject(err);
            }
            if (result) {
                resolve(true);
            }
        });
    })
}

function updateUserPasswordUsingEmail(email, password) {
    return new Promise((resolve, reject) => {

        let sql = `UPDATE t_users SET password='${password}' WHERE email='${email}'`;

        dbConn.query(sql, function (err, result) {
            if (err) {
                reject(err);
            }
            if (result) {
                resolve(true);
            }
        });
    })
}

function getUserinfo(id, fields = '*') {
    return new Promise((resolve, reject) => {
        let sql = `select ${fields} from t_users where id='${id}'`;
        dbConn.query(sql, function (err, result) {
            if (err) {
                reject(err);
            }
            if (result) {
                resolve(result);
            }
        });
    });
}

function getNotarizedFilesWithTransId(tranx_id) {
    return new Promise((resolve, reject) => {
        let sql_checkAlreadyExists = `SELECT * FROM t_notarized_files WHERE trx_id='${tranx_id}'`;

        dbConn.query(sql_checkAlreadyExists, function (err, result) {
            if (err) {
                reject(err)
            }
            if (result[0]) {
                resolve(result)
            } else {
                result = [];
                resolve(result)
            }
        });
    });
}

module.exports = { 
    registerUser, 
    isValidateLogin, 
    getUserFileCount, 
    getNotarizedFiles, 
   // getNotarizedFiles_by_user,
    addUserFiles, 
    verifyUser, 
    updateUserWalletInfo, 
    updateUserfile, 
    getUserinfo,
    updateUserProfile,
    updateUserPassword,
    isUserExists,
    updateUserPasswordUsingEmail,
    deleteUserfile,
    getNotarizedFilesWithTransId
 }
