const session = require('express-session');

function isLoggedIn(req) {
    if (req.session.loggedInUser)
        return true;
    else
        return false;
}

function setUserSession(req, user_id) {
    return new Promise((resolve, reject) => {
        if (!req.session.loggedInUserId) {
            req.session.loggedInUser = user_id;
            req.session.save(err => {
                if (!err) {
                    resolve(true);
                }
                if (err) {
                    reject(err);
                }
            });
        }
    });
}

function getUserSessionUserId(req) {
    return req.session.loggedInUser;
}

function abandonUserSession(req) {
    return new Promise((resolve, reject) => {
        req.session.loggedInUser = null;
        req.session.save(err => {
            if (!err) {
                resolve(true);
            }
            if (err) {
                reject(err);
            }
        });
    });
}


function setNewUserEmail(req, newUserEmail) {
    return new Promise((resolve, reject) => {
        req.session.newUserEmail = newUserEmail;
        req.session.save(err => {
            if (!err) {
                resolve(true);
            }
            if (err) {
                reject(err);
            }
        });
    });
}

function abandonNewUserEmail(req) {
    return new Promise((resolve, reject) => {
        req.session.newUserEmail = null;
        req.session.save(err => {
            if (!err) {
                resolve(true);
            }
            if (err) {
                reject(err);
            }
        });
    });
}

function getNewUserEmail(req) {
    if (req.session.newUserEmail)
        return req.session.newUserEmail;
    else
        return "";
}

function setResetUserEmail(req, resetUserEmail) {
    return new Promise((resolve, reject) => {
        req.session.resetUserEmail = resetUserEmail;
        req.session.save(err => {
            if (!err) {
                resolve(true);
            }
            if (err) {
                reject(err);
            }
        });
    });
}

function abandonResetUserEmail(req) {
    return new Promise((resolve, reject) => {
        req.session.resetUserEmail = null;
        req.session.save(err => {
            if (!err) {
                resolve(true);
            }
            if (err) {
                reject(err);
            }
        });
    });
}

function getResetUserEmail(req) {
    if (req.session.resetUserEmail)
        return req.session.resetUserEmail;
    else
        return "";
}


module.exports = {
    isLoggedIn,
    setUserSession,
    getUserSessionUserId,
    abandonUserSession,
    setNewUserEmail,
    abandonNewUserEmail,
    getNewUserEmail,
    setResetUserEmail,
    abandonResetUserEmail,
    getResetUserEmail
}