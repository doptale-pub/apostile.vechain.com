var express = require('express');
const mailConfig = require('../env/db-config');

function sendMail(to_mail, subject, html_content) {

    const nodemailer = require("nodemailer");

    async function main() {

        let account = {
            "user": "mailer@peerfin.io",
            "pass": "t$wJP%jGF28da2C3"
        };
        let transporter = nodemailer.createTransport({
            host: "smtp.zoho.com",
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
                user: account.user,
                pass: account.pass
            }
        });
        let mailOptions = {
            from: '"Verify Mailer" <mailer@peerfin.io>', // sender address
            to: to_mail, // list of receivers
            subject: subject,
            html: html_content
        };
        let info = await transporter.sendMail(mailOptions)

        console.log("Message sent: %s", info);
    }

    main().catch(console.error);
}

module.exports = { sendMail }