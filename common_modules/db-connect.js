const mysql = require('mysql');
const dbConfig = require('../env/db-config');

var con = mysql.createConnection({
    host: dbConfig.DB_HOST,
    user: dbConfig.DB_USERNAME,
    password: dbConfig.DB_PASSWORD,
    database: dbConfig.DB_DATABASE
});

con.connect(function(err) {
    if (err) throw err;
});

module.exports = con;