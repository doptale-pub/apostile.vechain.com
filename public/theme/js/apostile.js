if (!window.connex) {
    // alert('Download from  <a target="_blank" href="https://env.vechain.org/">here</a>')
    if (window.confirm('Your browser is missing Connex environment. Install VeChain Sync or comet to continue. Cancel will load this website ')) {
        window.location.href = 'https://env.vechain.org/r/#/test/' + encodeURIComponent(location.href);
        //  window.open('https://env.vechain.org/', '_blank');
    };
}
function notarise(elm_hash, user_account) {
    return new Promise(function (resolve, reject) {
        if (window.connex) {
            var signingService = connex.vendor.sign('tx');
            signingService
                .signer(user_account) // Enforce signer
                .gas(100000)
                .link('https://connex.vecha.in/{txid}')
                .comment('Use  VET register the document');
            const transferABI = {
                "constant": false, "inputs": [
                    { "name": "_to", "type": "address" },
                    { "name": "_amount", "type": "uint256" }
                ],
                "name": "transfer",
                "outputs": [
                    { "name": "success", "type": "bool" }],
                "payable": false,
                "stateMutability":
                    "nonpayable",
                "type": "function"
            }
            const transferMethod = connex.thor.account(user_account).method(transferABI);
            const energyClause = transferMethod.asClause(user_account, '100000');
            signingService.request([
                {
                    to: user_account,
                    value: '10000000000000000',
                    data: '0x' + elm_hash,
                    comment: 'Register file hash to vchain'
                },
                {
                    comment: 'Register file hash to vchain',
                    ...energyClause
                }
            ]).then(result => {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        } else {
            reject(0);
        }
    });
};

function audit(filename,filehash) {
    return new Promise(function (resolve, reject) {
        if (window.connex) {
            tx_id = filename.substr(0, filename.lastIndexOf("."));
            console.log(tx_id);
            var transaction=connex.thor.transaction(tx_id);
            transaction.get().then(result=>{
                if(result.clauses[0].data == '0x'+filehash){               
                    resolve({'trx_id':tx_id,'trx_data':result,'is_valid':1,'message':'File hash matched with transaction record'})
                }else{
                    resolve({'trx_id':tx_id,'trx_data':result,'is_valid':0,'message':'File hash not matching!'})
                }              
            },function (error) {
                reject(error);
            });
        } else {
            reject(0);
        }
    })
}