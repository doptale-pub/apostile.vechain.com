(function(a) {
    a(window).on("load", function() {
        a(".loader").fadeOut();
        a("#preloader").delay(350).fadeOut("slow")
    });
    a("#toggle-btn").on("click", function(b) {
        b.preventDefault();
        a(this).toggleClass("active");
        a(".side-navbar").toggleClass("shrinked");
        a(".content-inner").toggleClass("active");
        if (a(window).outerWidth() > 1183) {
            if (a("#toggle-btn").hasClass("active")) {
                a(".navbar-header .brand-small").hide();
                a(".navbar-header .brand-big").show()
            } else {
                a(".navbar-header .brand-small").show();
                a(".navbar-header .brand-big").hide()
            }
        }
        if (a(window).outerWidth() < 1183) {
            a(".navbar-header .brand-small").show()
        }
    });
    a(function() {
        a(".side-navbar li a").click(function(b) {
            a(".collapse").collapse("hide")
        })
    });
    a(window).resize(function() {
        var b = a(this).height() - a(".header").height() + a(".main-footer").height();
        a(".d-scroll").height(b)
    });
    a(window).resize();
    a(window).resize(function() {
        a(".auto-scroll").height(a(window).height() - 130)
    });
    a(window).trigger("resize");
    a(function() {
        a(window).scroll(function() {
            if (a(this).scrollTop() > 350) {
                a(".go-top").fadeIn(100)
            } else {
                a(".go-top").fadeOut(200)
            }
        });
        a(".go-top").click(function(b) {
            b.preventDefault();
            a("html, body").animate({
                scrollTop: 0
            }, 800)
        })
    });
    a(".checkbox").click(function() {
        a(this).toggleClass("is-checked")
    });
    a("#check-all").change(function() {
        a("input:checkbox").prop("checked", a(this).prop("checked"))
    });
    a("a.remove").on("click", function(b) {
        b.preventDefault();
        a(this).parents(".col-remove").fadeOut(500)
    });
    
    a("#search").on("click", function(b) {
        b.preventDefault();
        a(".search-box").slideDown()
    });
    a(".dismiss").on("click", function() {
        a(".search-box").slideUp()
    });
    a(".dropdown").on("show.bs.dropdown", function(b) {
        a(this).find(".dropdown-menu").first().stop(true, true).slideDown(300)
    });
    a(".dropdown").on("hide.bs.dropdown", function(b) {
        a(this).find(".dropdown-menu").first().stop(true, true).slideUp(300)
    });
    a(".widget-options > .dropdown, .actions > .dropdown, .quick-actions > .dropdown").hover(function() {
        a(this).find(".dropdown-menu").stop(true, true).delay(100).fadeIn(350)
    }, function() {
        a(this).find(".dropdown-menu").stop(true, true).delay(100).fadeOut(350)
    });
    a(function() {
        a(".open-sidebar").on("click", function(b) {
            b.preventDefault();
            a(".off-sidebar").addClass("is-visible")
        });
        a(".off-sidebar").on("click", function(b) {
            if (a(b.target).is(".off-sidebar") || a(b.target).is(".off-sidebar-close")) {
                a(".off-sidebar").removeClass("is-visible");
                b.preventDefault()
            }
        })
    });
    a(function() {
        a("#delay-modal").on("show.bs.modal", function() {
            var b = a(this);
            clearTimeout(b.data("hideInterval"));
            b.data("hideInterval", setTimeout(function() {
                b.modal("hide")
            }, 2500))
        })
    })
})(jQuery);