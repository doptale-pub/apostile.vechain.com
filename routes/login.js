var express = require('express');
const session = require('express-session');
const dbOperations = require('../common_modules/db-operations');
const sessionManagement = require('../common_modules/session-management');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  if (sessionManagement.isLoggedIn(req)) {
    res.redirect('/home');
  }else
  res.render('login', {
    title: 'Login',
    login_error: ""
  });
});

router.post('/', function (req, res) {
  const email = req.body.email;
  const password = req.body.password;
  dbOperations.isValidateLogin(email, password)
    .then((result) => {
       if (result==null) {
        res.render('login', {
          title: 'Login',
          login_error: "The username or password you entered is incorrect."
        });
      } else if(result.id !==0 && result.is_verified===0){      
        sessionManagement.setNewUserEmail(req, email)
        res.redirect('register/confirm');
      } else if(result.id !==0 && result.is_verified!==0 && result.wallet_address==null){      
        sessionManagement.setUserSession(req, result.id)
        .then(() => {
          res.redirect('register/walletinfo');    
        }, (err) => {
          console.log(err);
        });
      
      } else {      
        sessionManagement.setUserSession(req, result.id)
          .then(() => {
            res.redirect('/home');
          }, (err) => {
            console.log(err);
          });
      }
    }, (err) => {
      console.log(err);
    })
});


module.exports = router;