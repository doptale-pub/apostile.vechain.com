var express = require('express');
var router = express.Router();
const emailLib = require('../common_modules/email-sending');
const sessionManagement = require('../common_modules/session-management');
const dbOperations = require('../common_modules/db-operations');
var jwt = require('jwt-simple');
var password_reset_message_success='';
var password_reset_message_failure='';
var password_update_message='';

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('forget_pass', { title: 'Forget password',
          password_reset_message_success:'',
          password_reset_message_failure:''
  });
});

router.post('/', function (req, res, next) {
  const email = req.body.email,
  host = req.get('host');

  dbOperations.isUserExists(email)
    .then((isSuccess) => {
      if (isSuccess) {
        sendPasswordResetMail(host, email);
        password_reset_message_success="Password reset mail sent to your mail address"
        password_reset_message_failure='';

        res.render('forget_pass', { title: 'Forget password',
          password_reset_message_success:password_reset_message_success,
          password_reset_message_failure:password_reset_message_failure
        });
      } else {
        password_reset_message_success=''
        password_reset_message_failure="email address does not exist in system"
        res.render('forget_pass', { title: 'Forget password',
          password_reset_message_success:password_reset_message_success,
          password_reset_message_failure:password_reset_message_failure
        });
      }
    }, (err) => {
      console.log(err);
    });

});

router.get('/reset', function (req, res, next) {
  var token = req.query.token;
  var data = jwt.decode(token, "fe1a1915a379f3be5394b64d14794932");
  var email =data.email;
  sessionManagement.setResetUserEmail(req, email)
  .then(() => {
    res.render('reset_password', {     
      title: 'Reset Password', password_update_message:''
    });
  }, (err) => {
    console.log(err);
  });
});

router.post('/reset', function (req, res, next) {
  const password = req.body.password;
  var savedemail = sessionManagement.getResetUserEmail(req); 
  dbOperations.updateUserPasswordUsingEmail(savedemail, password)
      .then(() => {
        password_update_message="User password updated successfully"
        res.render('reset_password', { title: 'Reset Password', password_update_message:password_update_message });
      }, (err) => {
        console.log(err);
      });
});

function sendPasswordResetMail(host, to_email) {
  var payload = {};
  payload.email = to_email;
  payload.expiry = new Date(new Date().getTime() + 24 * 60 * 60 * 1000); //set expire 24 hr

  var token = jwt.encode(payload, "fe1a1915a379f3be5394b64d14794932");
  var link = "http://" + host + "/recovery/reset?token=" + token;
  var subject = "Reset Your Password";
  var html_content = "Hello,<br> Please Click on the link to reset your password.<br><a href=" + link + ">Click here to reset</a>"
  emailLib.sendMail(to_email, subject, html_content);
}

module.exports = router;
