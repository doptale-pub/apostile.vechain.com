var express = require('express');
var router = express.Router();
const dbOperations = require('../common_modules/db-operations');
const sessionManagement = require('../common_modules/session-management');
var wallet_update_message='';
var profile_update_message='';
var password_update_message='';

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('0');
});
router.get('/change_wallet', function (req, res, next) {
  if (sessionManagement.isLoggedIn(req)) {
    var user_id = sessionManagement.getUserSessionUserId(req);
    Promise.all([ dbOperations.getUserinfo(user_id, 'wallet_address')])
      .then((result) => {
        res.render('profile/wallet_info_change', { title: 'Change wallet info' ,wallet_address: result[0][0].wallet_address, wallet_update_message:""});
      }, (err) => {
        console.log(err);
      });
  }
  else {
    res.redirect('/login');
  }
});

router.post('/change_wallet', function (req, res, next) {
  if (sessionManagement.isLoggedIn(req)) {
    var user_id = sessionManagement.getUserSessionUserId(req);
    const wallet_address = req.body.wallet_address;

    dbOperations.updateUserWalletInfo(user_id, wallet_address)
      .then(() => {
        wallet_update_message="User wallet updated successfully"
        res.render('profile/wallet_info_change', { title: 'Change wallet info' ,wallet_address: wallet_address, wallet_update_message:wallet_update_message});
      }, (err) => {
        console.log(err);
      });
  } else {
    res.redirect('/login');
  }
});

router.get('/profile', function (req, res, next) {
  if (sessionManagement.isLoggedIn(req)) {
    var user_id = sessionManagement.getUserSessionUserId(req);
    Promise.all([dbOperations.getUserinfo(user_id, 'fullname,email,is_verified')])
      .then((result) => {
        res.render('profile/profile_edit_view', { title: 'Change wallet info', user_info: result[0][0],
        profile_update_message:profile_update_message,password_update_message:password_update_message });

      }, (err) => {
        console.log(err);
      });
  }
  else {
    res.redirect('/login');
  }
});



router.post('/update_profile', function (req, res, next) {
  if (sessionManagement.isLoggedIn(req)) {
    var user_id = sessionManagement.getUserSessionUserId(req);
    const fullname = req.body.fullname;

    dbOperations.updateUserProfile(user_id, fullname)
      .then(() => {
          password_update_message='';
          profile_update_message="User profile updated successfully"
          Promise.all([dbOperations.getUserinfo(user_id, 'fullname,email,is_verified')])
          .then((result) => {
            res.render('profile/profile_edit_view', { title: 'Change wallet info', user_info: result[0][0],
            profile_update_message:profile_update_message,password_update_message:password_update_message });
          }, (err) => {
            console.log(err);
          });
      }, (err) => {
        console.log(err);
      });
  } else {
    res.redirect('/login');
  }
});

router.post('/update_password', function (req, res, next) {
  if (sessionManagement.isLoggedIn(req)) {
    var user_id = sessionManagement.getUserSessionUserId(req);
    const password = req.body.password;

    dbOperations.updateUserPassword(user_id, password)
      .then(() => {
        profile_update_message='';
        password_update_message="User password updated successfully"
        Promise.all([dbOperations.getUserinfo(user_id, 'fullname,email,is_verified')])
        .then((result) => {
          res.render('profile/profile_edit_view', { title: 'Change wallet info', user_info: result[0][0],
          profile_update_message:profile_update_message,password_update_message:password_update_message });
        }, (err) => {
          console.log(err);
        });
      }, (err) => {
        console.log(err);
      });
  } else {
    res.redirect('/login');
  }
});




router.get('/logout', function (req, res, next) {
  sessionManagement.abandonUserSession(req)
    .then(() => {
      res.redirect('/login');
    }, (err) => {
      console.log(err);
    });
});


module.exports = router;
