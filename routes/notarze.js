var express = require('express');
var router = express.Router();
const dbOperations = require('../common_modules/db-operations');
const sessionManagement = require('../common_modules/session-management');
const multer = require('multer');
const upload = multer({ dest: __dirname + '/files/' });
const fs = require('fs');


function randomKey(n) {
    const alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        randomAlpha = alpha.split("").sort(function () {
            return 0.5 - Math.random();
        });
    return randomAlpha.splice(0, n).join("");
}

/* GET home page. */
router.get('/', function (req, res, next) {
    if (sessionManagement.isLoggedIn(req)) {
        res.redirect('notarize/list');
    }
    else {
        res.redirect('/login');
    }
});

router.get('/create', function (req, res, next) {
    if (sessionManagement.isLoggedIn(req)) {
        var user_id = sessionManagement.getUserSessionUserId(req);
        Promise.all([dbOperations.getUserinfo(user_id, 'wallet_address')]) //dbOperations.getNotarizedFiles(user_id) ,
            .then((result) => {
                if (result[0][0].wallet_address == '' || !result[0][0].wallet_address) {
                    res.redirect('/user/change_wallet');
                } else {
                    res.render('notarize/create', { title: 'Notarized Files', wallet_address: result[0][0].wallet_address });
                }
            }, (err) => {
                console.log(err);
            });
        // dbOperations.getNotarizedFiles(user_id)
        //     .then((result) => {
        //         res.render('notarize/create', { title: 'Notarized Files', files: result });
        //     }, (err) => {
        //         console.log(err);
        //     });
    }
    else {
        res.redirect('/login');
    }
});

router.get('/list', function (req, res, next) {
    if (sessionManagement.isLoggedIn(req)) {
        var user_id = sessionManagement.getUserSessionUserId(req);
        dbOperations.getNotarizedFiles(user_id)
            .then((result) => {
                res.render('notarize/list', { title: 'Notarized Files', files: result });
            }, (err) => {
                console.log(err);
            });
    }
    else {
        res.redirect('/login');
    }
});

router.post('/file-upload', upload.any(), function (req, res, next) {
    if (sessionManagement.isLoggedIn(req)) {
        var user_id = sessionManagement.getUserSessionUserId(req);
        var jsonData = [];
        var uploaded_file = req.files[0],
            uploaded_path = uploaded_file.path,
            uploaded_fileName = uploaded_file.originalname;
        final_fileName = uploaded_fileName.replace(/\.[^/.]+$/, "") + randomKey(10) + "." + uploaded_fileName.split('.').pop(),
            final_Path = __dirname + '/files/' + final_fileName;
        //create hash for file
        var hashFiles = require('hash-files');
        var options = {
            "files": [uploaded_file.path],
            "algorithm": "sha256"
        };

        hashFiles(options, function (error, file_hash) {
            // hash will be a string if no error occurred  
            fs.rename(uploaded_path, final_Path, function (err) {
                if (err) {
                    console.log(err);
                    res.json({
                        error: err
                    });
                } else {
                    dbOperations.addUserFiles(user_id, final_fileName, file_hash)
                        .then((insertId) => {
                            if (insertId > 0) {
                                jsonData.push({ id: insertId, file_hash: file_hash, filename: final_fileName });
                                res.json({ artifacts: jsonData });
                            }
                        }, (err) => {
                            console.log(err);
                        });
                }
            });
        });


    }
    else {
        res.redirect('/login');
    }
});

router.get('/:id/download', function (req, res, next) {
    if (sessionManagement.isLoggedIn(req)) {
        var transaction_id = req.params.id;
        dbOperations.getNotarizedFilesWithTransId(transaction_id)
            .then((result) => {
                var fileName = result[0].filename;
                var filePath = "routes/files/" + fileName;
                var downloadFileName = result[0].trx_id + '.' + fileName.split('.').pop();
                res.download(filePath, downloadFileName);
            }, (err) => {
                console.log(err);
            });

    }
    else {
        res.redirect('/login');
    }
});

//Verfiy
router.get('/verfiy', function (req, res, next) {
    if (sessionManagement.isLoggedIn(req)) {
        res.render('notarize/verifiy', { title: 'Audit a file' });
    }
    else {
        res.redirect('/login');
    }
});

let verf_upload = multer({ dest: __dirname + '/temp/' });

router.post('/verfiy', verf_upload.any(), function (req, res, next) {
    if (sessionManagement.isLoggedIn(req)) {
        var jsonData = [];
        var uploaded_file = req.files[0],
            uploaded_path = uploaded_file.path,
            uploaded_fileName = uploaded_file.originalname;
        var hashFiles = require('hash-files');
        // console.log(uploaded_path,uploaded_fileName);
        var options = {
            "files": [uploaded_path],
            "algorithm": "sha256"
        };
        var file_hash = '';
        hashFiles(options, function (error, hash) {
            // hash will be a string if no error occurred
            res.json({ artifacts: { file_hash: hash, file_name: uploaded_fileName } });
        });
    }
    else {
        res.redirect('/login');
    }
});


router.post('/reg_file', function (req, res, next) {
    if (sessionManagement.isLoggedIn(req)) {
        var user_id = sessionManagement.getUserSessionUserId(req);

        dbOperations.updateUserfile(user_id, req.body.file_id, req.body.transaction_id)
            .then((result) => {
                res.json({ "data": "1", "msg": "File updated" });
            }, (err) => {
                res.json({ "data": "0", "msg": "File update failed" });
                console.log(err);
            });

    } else {
        res.redirect('/login');
    }
});

router.post('/delete_file', function (req, res, next) {
    if (sessionManagement.isLoggedIn(req)) {
        var user_id = sessionManagement.getUserSessionUserId(req);
        var file_id=req.body.file_id;
        dbOperations.deleteUserfile(user_id, file_id)
            .then((result) => {
                res.json({ "data": "1", "msg": "File deleted" });
            }, (err) => {
                res.json({ "data": "0", "msg": "File deleted failed" });
                console.log(err);
            });

    } else {
        res.redirect('/login');
    }
});

module.exports = router;