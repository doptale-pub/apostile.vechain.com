var express = require('express');
var router = express.Router();
const dbOperations = require('../common_modules/db-operations');
const sessionManagement = require('../common_modules/session-management');

/* GET home page. */
router.get('/', function (req, res, next) {

  if (sessionManagement.isLoggedIn(req)) {
    var user_id = sessionManagement.getUserSessionUserId(req);
    Promise.all([dbOperations.getUserFileCount(user_id),dbOperations.getUserinfo(user_id,'wallet_address')])
      .then((result) => {
        res.render('home', { title: 'Home', FileCount: result[0],wallet_address: result[1][0].wallet_address  });
      }, (err) => {
        console.log(err);
      });


    // dbOperations.getUserFileCount(user_id)
    //   .then((fileCount) => {

    //   }, (err) => {
    //     console.log(err);
    //   });
  }
  else {
    res.redirect('/login');
  }
});
module.exports = router;