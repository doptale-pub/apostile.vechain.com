var express = require('express');
const dbOperations = require('../common_modules/db-operations');
const emailLib = require('../common_modules/email-sending');
const sessionManagement = require('../common_modules/session-management');
var router = express.Router();
var jwt = require('jwt-simple');

/* GET home page. */
router.get('/', function (req, res, next) {
  if (sessionManagement.isLoggedIn(req)) {
    res.redirect('/home');
  } else
    res.render('register', {
      title: 'Express',
      signup_error: ""
    });
});

router.post('/', function (req, res, next) {

  const fullname = req.body.fullname,
    email = req.body.email,
    password = req.body.password,
    host = req.get('host');

  dbOperations.registerUser(fullname, email, password)
    .then((isSuccess) => {
      if (isSuccess) {
        sendMailConfirmation(host, email);
        sessionManagement.setNewUserEmail(req, email)
          .then(() => {
            res.redirect('register/confirm');
          }, (err) => {
            console.log(err);
          });
      } else {
        res.render('register', {
          title: 'User Registration',
          signup_error: "email already exists"
        });
      }
    }, (err) => {
      console.log(err);
    });
});

router.get('/confirm', function (req, res, next) {
  var email = sessionManagement.getNewUserEmail(req);
  res.render('confirm', {
    title: 'Confirm Registration',
    email: email
  });
});

router.get('/walletinfo', function (req, res, next) {
  
  if (sessionManagement.isLoggedIn(req)) {
    var user_id = sessionManagement.getUserSessionUserId(req);
    Promise.all([ dbOperations.getUserinfo(user_id, 'wallet_address')])
      .then((result) => {
        if(result[0][0].wallet_address && result[0][0].wallet_address!='') {
          res.redirect('/user/change_wallet');
        }else{
          res.render('walletinfo', {
            title: 'Update Wallet Information',
            email: user_id
          });
        }

       // res.render('profile/wallet_info_change', { title: 'Change wallet info' ,wallet_address: result[0][0].wallet_address, wallet_update_message:""});
        
      }, (err) => {
        console.log(err);
      });
    // if(wallet_address !=''){
    // var email = sessionManagement.getNewUserEmail(req);
    // res.render('walletinfo', {
    //   title: 'Update Wallet Information',
    //   email: email
    // });}
  }else{
    res.redirect('/login');
  }
});


router.post('/walletinfo', function (req, res, next) {
  if (sessionManagement.isLoggedIn(req)) {
    var user_id = sessionManagement.getUserSessionUserId(req);
    const wallet_address = req.body.wallet_address;
    dbOperations.updateUserWalletInfo(user_id, wallet_address)
      .then(() => {
        res.redirect('/home');
      }, (err) => {
        console.log(err);
      });
  } else {
    res.redirect('/login');
  }
});

router.get('/verify', function (req, res) {

  var token = req.query.token;
  var data = jwt.decode(token, "fe1a1915a379f3be5394b64d14794932");
  var email = data.email;
  dbOperations.verifyUser(email)
    .then(() => {
      res.render('verify', { title: 'Verify', email: email });
    }, (err) => {
      console.log(err);
    })
});


function sendMailConfirmation(host, to_email) {
  var payload = {};
  payload.email = to_email;
  payload.expiry = new Date(new Date().getTime() + 24 * 60 * 60 * 1000); //set expire 24 hr

  var token = jwt.encode(payload, "fe1a1915a379f3be5394b64d14794932");
  var link = "http://" + host + "/register/verify?token=" + token;
  var subject = "Please confirm your Email account";
  var html_content = "Hello,<br> Please Click on the link to verify your email.<br><a href=" + link + ">Click here to verify</a>"
  emailLib.sendMail(to_email, subject, html_content);
}

module.exports = router;