var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const session = require('express-session');
consts = require('./bin/constants');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(session({secret: "secret!156132", resave: false, saveUninitialized: false, cookie: { secure: false }}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', require('./routes/login'));
app.use('/register', require('./routes/register'));
app.use('/login', require('./routes/login'));
app.use('/recovery', require('./routes/recovery'));
app.use('/user', require('./routes/users'));
app.use('/home', require('./routes/home'));
app.use('/notarize',  require('./routes/notarze'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
 // next(createError(404));
 res.render('err_404');
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
